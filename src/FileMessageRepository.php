<?php declare(strict_types=1);

namespace App;

use App\Interfaces\MessageRepositoryInterface;

class FileMessageRepository implements MessageRepositoryInterface
{
    /** @var string */
    private $storageDir;

    /**
     * FileMessageRepository constructor.
     * @param string $storageDir
     */
    public function __construct(string $storageDir = ".")
    {
        $this->storageDir = $storageDir;
    }

    public function save(Message $message): void
    {
        $fileName = $this->storageDir . DIRECTORY_SEPARATOR . $message->getTitle() . " " . $message->getDate(). ".txt";
        $file = fopen($fileName, 'w');

        fputs($file, "Title: " . $message->getTitle() . "\n");
        fputs($file, "Date: " . $message->getDate() . "\n");
        fputs($file, "Author: " . $message->getAuthor() . "\n");
        fputs($file, "Content: " . $message->getContent() . "\n");

        fclose($file);
    }
}
