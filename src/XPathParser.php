<?php declare(strict_types=1);

namespace App;

use App\Interfaces\ParserInterface;
use DOMDocument;
use DOMElement;
use DOMXPath;

class XPathParser implements ParserInterface
{
    const POST_ITEMS_XPATH = ".//div[@id='postlist']/ol/li[@id]";

    /** @var DOMDocument  */
    private $dom;

    public function __construct()
    {
        $this->dom = new DOMDocument();
    }

    /**
     * @param string $html
     * @return Message[]
     */
    public function parseHtml(string $html): array
    {
        $result = [];
        $this->dom->loadHTML($html);
        $xpath = new DomXPath($this->dom);

        $postItems = $xpath->query(self::POST_ITEMS_XPATH);

        /** @var DOMElement $postItem */
        foreach ($postItems as $postItem) {
            $dateNode = $xpath->query(".//span[@class='date']", $postItem);
            $date = (null !== $dateNode && $dateNode->length > 0) ? $dateNode->item(0)->nodeValue : "";

            $titleNode = $xpath->query(".//h2", $postItem);
            $title = (null !== $titleNode && $titleNode->length > 0) ? trim($titleNode->item(0)->textContent) : "";

            $authorNode = $xpath->query(".//a[@class='username offline popupctrl']", $postItem);
            $author = (null !== $authorNode && $authorNode->length > 0) ? $authorNode->item(0)->nodeValue : "";

            $contentNode = $xpath->query(".//blockquote[@class='postcontent restore ']", $postItem);
            $content = (null !== $contentNode && $contentNode->length > 0) ? trim($contentNode->item(0)->nodeValue) : "";

            $result[] = new Message($title, $author, $date, $content);
        }

        return $result;
    }
}