<?php declare(strict_types=1);

namespace App;

use App\Interfaces\ConfigurationInterface;
use App\Exceptions\InvalidConfigurationFileException;
use App\Exceptions\InvalidPasswordException;
use App\Exceptions\InvalidTopicUrlException;
use App\Exceptions\InvalidUserNameException;

class IniConfiguration implements ConfigurationInterface
{
    private $userName;
    private $password;
    private $topicUrl;

    /**
     * Configuration constructor.
     * @param $userName
     * @param $password
     * @param $topicUrl
     * @throws InvalidPasswordException
     * @throws InvalidTopicUrlException
     * @throws InvalidUserNameException
     */
    private function __construct($userName, $password, $topicUrl)
    {
        if (empty($userName)) {
            throw new InvalidUserNameException();
        }

        if (empty($password)) {
            throw new InvalidPasswordException();
        }

        if (false === filter_var($topicUrl, FILTER_VALIDATE_URL)) {
            throw new InvalidTopicUrlException();
        }

        $this->userName = $userName;
        $this->password = $password;
        $this->topicUrl = $topicUrl;
    }

    /**
     * @param string $filePath
     * @return ConfigurationInterface
     * @throws InvalidConfigurationFileException
     */
    public static function createFromConfigFile(string $filePath): ConfigurationInterface
    {
        $fileValues = parse_ini_file($filePath);

        if (false === $fileValues) {
            throw new InvalidConfigurationFileException();
        }

        return new self(
            $fileValues['userName'] ?: null,
            $fileValues['password'] ?: null,
            $fileValues['topicUrl'] ?: null
        );
    }

    public function getUsername(): string
    {
        return $this->userName;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getTopicUrl(): string
    {
        return $this->topicUrl;
    }
}