<?php declare(strict_types=1);

namespace App;

class Message
{
    /** @var string */
    private $title;

    /** @var string */
    private $author;

    /** @var string */
    private $date;

    /** @var string */
    private $content;

    /**
     * Message constructor.
     * @param string $title
     * @param string $author
     * @param string $date
     * @param string $content
     */
    public function __construct(string $title, string $author, string $date, string $content)
    {
        $this->title = $title;
        $this->author = $author;
        $this->date = $date;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}