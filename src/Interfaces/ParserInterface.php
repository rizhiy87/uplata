<?php declare(strict_types=1);

namespace App\Interfaces;

use App\Message;

interface ParserInterface
{
    /**
     * @param string $html
     * @return Message[]
     */
    public function parseHtml(string $html): array;
}