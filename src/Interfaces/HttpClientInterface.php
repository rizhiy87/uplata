<?php declare(strict_types=1);

namespace App\Interfaces;

use App\Exceptions\HttpException;

interface HttpClientInterface
{
    /**
     * @param string $login
     * @param string $password
     * @throws HttpException
     */
    public function login(string $login, string $password): void;

    /**
     * @param string $topicUrl
     * @return string
     * @throws HttpException
     */
    public function getContent(string $topicUrl): string;
}