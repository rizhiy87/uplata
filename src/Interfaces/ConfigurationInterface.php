<?php declare(strict_types=1);

namespace App\Interfaces;

use App\Exceptions\InvalidConfigurationFileException;

interface ConfigurationInterface
{
    /**
     * @param string $filePath
     * @return ConfigurationInterface
     * @throws InvalidConfigurationFileException
     */
    public static function createFromConfigFile(string $filePath): ConfigurationInterface;

    public function getUsername(): string;
    public function getPassword(): string;
    public function getTopicUrl(): string;
}