<?php declare(strict_types=1);

namespace App\Interfaces;

use App\Message;

interface MessageRepositoryInterface
{
    public function save(Message $message): void;
}
