<?php declare(strict_types=1);

namespace App;

use App\Exceptions\HttpException;
use App\Interfaces\HttpClientInterface;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;

class GuzzleHttpClient implements HttpClientInterface
{
    /** @var ClientInterface */
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'cookies' => true
        ]);
    }

    /**
     * @param string $login
     * @param string $password
     * @throws HttpException
     */
    public function login(string $login, string $password): void
    {
        // form_params got from browser's web console during login

        try {
            $this->client->post('https://forumodua.com/login.php?do=login', [
                'form_params' => [
                    'do' => 'login',
                    'securitytoken' => 'guest',
                    'url' => 'https://forumodua.com/login.php?do=login',
                    'vb_login_username' => $login,
                    'vb_login_password' => $password,
                    'cookieuser' => '1',
                ],
            ]);
        } catch (RequestException $e) {
            throw new HttpException();
        }

//        As we set cookies = true in line 17 of this file, auth cookie returned on previous request
//        will be set into $client and used on further requests
    }

    /**
     * @param string $topicUrl
     * @return string
     * @throws HttpException
     */
    public function getContent(string $topicUrl): string
    {
        try {
            $response = $this->client->get($topicUrl);
        } catch (RequestException $e) {
            throw new HttpException();
        }

        return $response->getBody()->getContents();
    }
}