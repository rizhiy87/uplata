<?php declare(strict_types=1);

namespace App\Exceptions;

class InvalidPasswordException extends InvalidConfigurationFileException
{
    private $message = 'Password configuration is invalid';
}