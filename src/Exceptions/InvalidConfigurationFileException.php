<?php declare(strict_types=1);

namespace App\Exceptions;

class InvalidConfigurationFileException extends \Exception
{
    protected $message = 'Configuration file is incorrect';
}