<?php declare(strict_types=1);

namespace App\Exceptions;

class HttpException extends \Exception
{
    protected $message = 'Something wrong making HTTP request';
}