<?php declare(strict_types=1);

namespace App\Exceptions;

class InvalidUserNameException extends InvalidConfigurationFileException
{
    private $message = 'UserName configuration is invalid';
}