<?php declare(strict_types=1);

namespace App\Exceptions;

class InvalidTopicUrlException extends InvalidConfigurationFileException
{
    private $message = 'Topic URL is not valid configuration is invalid or is not a valid url';
}