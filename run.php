<?php
libxml_use_internal_errors(true);

use App\Exceptions\InvalidConfigurationFileException;
use App\FileMessageRepository;
use App\GuzzleHttpClient;
use App\IniConfiguration;
use App\XPathParser;

include "vendor/autoload.php";

try {
    $configuration = IniConfiguration::createFromConfigFile(__DIR__ . DIRECTORY_SEPARATOR . 'config.ini');
} catch (InvalidConfigurationFileException $e) {
    echo "Error: " . $e->getMessage();
    return;
}

$client = new GuzzleHttpClient();
$parser = new XPathParser();
$repository = new FileMessageRepository(__DIR__ . DIRECTORY_SEPARATOR . 'data');

try {
    $client->login('RJ45', 'sanders');
    echo "Login success.\n";
} catch (\App\Exceptions\HttpException $e) {
    echo "Login failed. " . $e->getMessage() . "\n";
}

try {
    $content = $client->getContent($configuration->getTopicUrl());
} catch (\App\Exceptions\HttpException $e) {
    echo "Error on parsing page. " . $e->getMessage();
    return;
}

$messages = $parser->parseHtml($content);

foreach ($messages as $message) {
    $repository->save($message);
}

echo "Done\n";